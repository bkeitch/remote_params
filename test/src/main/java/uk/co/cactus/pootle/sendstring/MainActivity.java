package uk.co.cactus.pootle.sendstring;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.MenuItem;
import android.widget.*;
import android.os.StrictMode;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;

import java.io.PrintWriter;
import java.net.*;
import java.net.UnknownHostException;


public class MainActivity extends ActionBarActivity {
    private String mHost = "129.67.92.87";
    private int mPort = 7000;

    TextView result;
    EditText inputName;
    EditText inputValue;
    EditText inputHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputHost = (EditText) findViewById(R.id.host);
        inputName = (EditText) findViewById(R.id.name);
        inputValue = (EditText) findViewById(R.id.value);
        result = (TextView) findViewById(R.id.result);
        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }

        //Listening to button event
        btnSubmit.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                saveValue(arg0);
                //Starting a new Intent

//                Intent nextScreen = new Intent(getApplicationContext(), SettingsActivity.class);
//
//                //Sending data to another Activity
//                nextScreen.putExtra("name", inputName.getText().toString());
//                nextScreen.putExtra("email", inputEmail.getText().toString());
//
//                Log.e("n", inputName.getText()+"."+ inputEmail.getText());
//
//                startActivity(nextScreen);

            }
        });

    }
    public void saveValue(View clickedButton) {
        try {
            Socket socket = new Socket(inputHost.getText().toString(), mPort);
            PrintWriter out = new PrintWriter( socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader( new InputStreamReader(socket.getInputStream()));
//            out.printf("HEAD / HTTP/1.1\r\n");
//            out.printf("Host: google.com \r\n");
//            out.printf("Connection: close \r\n\r\n");
            String mesg = inputName.getText() + " " + inputValue.getText();
            out.println((char)(mesg.length()) + mesg);
            String line = in.readLine();
            result.setText(line);
            socket.close();

        } catch (UnknownHostException uhe) {
            result.setText("Unknown host : " + mHost);
            uhe.printStackTrace();

        } catch (IOException ioe) {
            result.setText("IO Exception : " + ioe);
            ioe.printStackTrace();

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
